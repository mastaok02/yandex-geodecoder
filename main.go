package main

import (
	"fmt"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/twinj/uuid"
	"github.com/xuri/excelize/v2"
	"gitlab.com/mastaok02/yandex-geodecoder/request"
)

const sheetName = "Лист1"
const apiKey = "47e1c672-1825-40de-9751-7c4c008d728f"

func main() {

	app := fiber.New(fiber.Config{
		Prefork:       true,
		CaseSensitive: true,
		StrictRouting: true,
		ServerHeader:  "Fiber",
		AppName:       "Yandex Geo Decoder v1.0.1",
		BodyLimit:     30 * 1024 * 1024,
	})

	app.Use(cors.New())
	app.Use(logger.New())

	app.Static("/uploads", "uploads")

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("aaa")
	})

	app.Post("/", func(c *fiber.Ctx) error {
		u := uuid.NewV4()
		file, err := c.FormFile("document")
		if err != nil {
			return err
		}
		fileName := file.Filename[:len(file.Filename)-6] + u.String() + ".xlsx"
		dest := fmt.Sprintf("./uploads/%s", fileName)

		if err := c.SaveFile(file, dest); err != nil {
			return err
		}

		f, err := excelize.OpenFile(dest)
		if err != nil {
			fmt.Println(err)
			return err
		}
		defer func() {
			// Close the spreadsheet.
			if err := f.Close(); err != nil {
				fmt.Println(err)
			}
		}()

		cols, err := f.GetCols(sheetName)
		if err != nil {
			fmt.Println(err)
			return err
		}

		ticker := 0
		for j, rowCell := range cols[0] {
			fmt.Print(j, rowCell, "\n")
			ydxResp, err := request.Make(rowCell, apiKey)
			if err != nil {
				panic(err)
			}
			positionList := make([]string, 0)
			for _, v := range ydxResp.Response.GeoObjectCollection.FeatureMember {
				positionList = append(positionList, v.GeoObject.Point.Pos)
			}

			if err := f.SetCellValue(sheetName, fmt.Sprintf("B%d", j+1), strings.Join(positionList[:], ",")); err != nil {
				return err
			}

			ticker++
			// if ticker %20 == 0 {
			// 	time.Sleep(time)
			// }
		}
		if err := f.SaveAs(dest); err != nil {
			fmt.Println(err)
		}

		return c.Download(dest)
	})
	app.Listen("localhost:3000")
}
