package request

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/mastaok02/yandex-geodecoder/model"
)

func Make(geocode, apiKey string) (*model.HttpResponse, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://geocode-maps.yandex.ru/1.x/", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept", "application/json")

	q := req.URL.Query()
	q.Add("apikey", apiKey) //"47e1c672-1825-40de-9751-7c4c008d728f"
	q.Add("geocode", geocode)
	q.Add("format", "json")
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	result := new(model.HttpResponse)
	if err := json.Unmarshal(body, result); err != nil {
		return nil, err
	}
	return result, nil
}
