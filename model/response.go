package model

type HttpResponse struct {
	Response struct {
		GeoObjectCollection struct {
			FeatureMember []struct {
				GeoObject struct {
					Point struct {
						Pos string `json:"pos"`
					} `json:"Point"`
					BoundedBy struct {
						Envelope struct {
							LowerCorner string `json:"lowerCorner"`
							UpperCorner string `json:"upperCorner"`
						} `json:"Envelope"`
					} `json:"boundedBy"`
					Description      string `json:"description"`
					MetaDataProperty struct {
						GeocoderMetaData struct {
							Address struct {
								Components []struct {
									Kind string `json:"kind"`
									Name string `json:"name"`
								} `json:"Components"`
								CountryCode string `json:"country_code"`
								Formatted   string `json:"formatted"`
								PostalCode  string `json:"postal_code"`
							} `json:"Address"`
							AddressDetails struct {
								Country struct {
									AddressLine        string `json:"AddressLine"`
									AdministrativeArea struct {
										AdministrativeAreaName string `json:"AdministrativeAreaName"`
										Locality               struct {
											LocalityName string `json:"LocalityName"`
											Thoroughfare struct {
												Premise struct {
													PostalCode struct {
														PostalCodeNumber string `json:"PostalCodeNumber"`
													} `json:"PostalCode"`
													PremiseNumber string `json:"PremiseNumber"`
												} `json:"Premise"`
												ThoroughfareName string `json:"ThoroughfareName"`
											} `json:"Thoroughfare"`
										} `json:"Locality"`
										SubAdministrativeArea struct {
											Locality struct {
												LocalityName string `json:"LocalityName"`
												Thoroughfare struct {
													Premise struct {
														PostalCode struct {
															PostalCodeNumber string `json:"PostalCodeNumber"`
														} `json:"PostalCode"`
														PremiseNumber string `json:"PremiseNumber"`
													} `json:"Premise"`
													ThoroughfareName string `json:"ThoroughfareName"`
												} `json:"Thoroughfare"`
											} `json:"Locality"`
											SubAdministrativeAreaName string `json:"SubAdministrativeAreaName"`
										} `json:"SubAdministrativeArea"`
									} `json:"AdministrativeArea"`
									CountryName     string `json:"CountryName"`
									CountryNameCode string `json:"CountryNameCode"`
								} `json:"Country"`
							} `json:"AddressDetails"`
							Kind      string `json:"kind"`
							Precision string `json:"precision"`
							Text      string `json:"text"`
						} `json:"GeocoderMetaData"`
					} `json:"metaDataProperty"`
					Name string `json:"name"`
				} `json:"GeoObject"`
			} `json:"featureMember"`
			MetaDataProperty struct {
				GeocoderResponseMetaData struct {
					Found   string `json:"found"`
					Request string `json:"request"`
					Results string `json:"results"`
				} `json:"GeocoderResponseMetaData"`
			} `json:"metaDataProperty"`
		} `json:"GeoObjectCollection"`
	} `json:"response"`
}
